# Kainth, Vibhor
# 1001-420-430
# 2017-10-09
# Assignment_03_05

import numpy as np

""" --> Based on Sebastian Raschka's Book Python Machine Learning pg 37 - 38 Chapter 2
"""


class Adaline(object):
    """ Class to model an Adaptive Linear Neural Network
    """
    def __init__(self, eta=0.01, n_iter=50):
        self.eta = eta
        self.n_iter = n_iter

    def fit(self, X, y):
        """ Calculates the weights and biases against provided input
        :param X: training values
        :param y: training classes
        :return: Adaline object
        """
        self.w_ = np.zeros(1 + X.shape[1])
        # print("self.weights: ", self.weights)
        self.cost = list()
        # errors = list()
        for i in range(self.n_iter):
            errors = list()
            for row, tr_val in zip(X, y):
                output = self.net_input(row)
                err = tr_val - output
                errors.append(err)
                self.w_[1:] += 2 * self.eta * row.T * err
                self.w_[0] += 2 * self.eta * err
            # cost = np.average(np.array(errors) ** 2)
            # cost = (np.array(errors) ** 2).sum() / len(errors)
            # self.cost.append(cost)
        return self

    def net_input(self, X):
        """ Calculates the net input to the neural network
        :param X: training values
        :return: sum of dot product of X and weights, and bias
        """
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def activation(self, X):
        """ Applies linear activation function
        :param X: training classes
        :return: net input since linear activation
        """
        return self.net_input(X)

    def set_iters(self, n_iter):
        self.n_iter = n_iter

    def set_eta(self, eta):
        self.eta = eta

    def reset_weights(self, X):
        """ Resets the weights to zero
        :param X: training values
        :return: nothing
        """
        self.w_ = np.zeros(1 + X.shape[1])
