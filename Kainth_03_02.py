# Kainth, Vibhor
# 1001-420-430
# 2017-10-09
# Assignment_03_02

import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import numpy as np
import tkinter as tk
import Kainth_03_04 as k04
import Kainth_03_05 as k05
from sklearn.model_selection import train_test_split
matplotlib.use('TkAgg')


class DisplayActivationFunctions:
    """
    This class is for displaying activation functions for NN.
    Farhad Kamangar 2017_08_26
    """

    def __init__(self, root, master, *args, **kwargs):
        self.master = master
        self.root = root
        #########################################################################
        #  Set up the constants and default values
        #########################################################################
        self.xmin = 1
        self.xmax = 10
        self.ymin = 0
        self.ymax = 2
        self.learning_rate = 0.1
        self.delayed_elems = 10
        self.train_size = 0.8
        self.batch_size = 100
        self.iters = 0
        self.num_iters = 10
        self.bias = 0
        self.mae_price = []
        self.mae_volume = []
        self.mse_price = []
        self.mse_volume = []
        # Data import
        self.inp_vec = k04.input_data("Data/stock_data.csv")
        self.price_var = self.inp_vec[:, 0]
        self.volume_var = self.inp_vec[:, 1]
        self.X_train, self.X_test, self.price_train, self.price_test, self.volume_train, self.volume_test = \
            train_test_split(self.inp_vec, self.price_var, self.volume_var, train_size=self.train_size)
        print("self.X_test.shape: ", self.price_test.shape)
        self.price_ada = k05.Adaline(n_iter=self.num_iters, eta=self.learning_rate)
        self.volume_ada = k05.Adaline(n_iter=self.num_iters, eta=self.learning_rate)
        # DEBUG
        # print("training_var: ", training_var)
        # print("self.training_classes: ", self.training_classes)
        print("self.X_train.shape: ", self.price_train.shape)
        # Initial activation function is hardlims
        self.activation_function = "Symmetrical Hard Limit"
        self.learning_method = "Filtered Learning"
        # self.weights = np.ones((self.y_train.shape[1], self.X_train.shape[1]))
        # self.weights.reshape((10, 784))
        self.weights = np.zeros(1 + self.X_train.shape[1])
        print("self.weights.shape: ", self.weights.shape)
        self.error = 100.0
        self.errors = {}
        self.errors[0] = 100.0
        #########################################################################
        #  Set up the plotting area
        #########################################################################
        self.plot_frame = tk.Frame(self.master)
        self.plot_frame.grid(row=0, column=0, columnspan=4, sticky=tk.N + tk.E + tk.S + tk.W)
        self.plot_frame.rowconfigure(0, weight=1)
        self.plot_frame.columnconfigure(0, weight=1)
        self.figure = plt.figure("")
        # self.figure = plt.subplot(121)
        self.mae_price_plt = self.figure.add_subplot(221)  # creates a subplot
        self.mae_volume_plt = self.figure.add_subplot(222)
        self.mse_price_plt = self.figure.add_subplot(223)
        self.mse_volume_plt = self.figure.add_subplot(224)
        self.axes = self.figure.gca()
        # self.mae_price_plt.set_xlabel('Number of Epochs')
        # self.mae_price_plt.set_ylabel('Error Rate (%)')
        # plt.set_title("Assignment 02 - Kainth")

        plt.xlim(self.xmin, self.xmax)
        plt.ylim(self.ymin, self.ymax)
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.root)

        self.plot_widget = self.canvas.get_tk_widget()
        self.plot_widget.grid(row=0, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        #########################################################################
        #  Set up the frame for sliders (scales)
        #########################################################################
        self.sliders_frame = tk.Frame(self.master)
        self.sliders_frame.grid(row=0, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        self.sliders_frame.rowconfigure(0, weight=1)
        self.sliders_frame.columnconfigure(0, weight=1, uniform='xx')
        # set up the sliders
        self.learning_rate_slider = tk.Scale(self.sliders_frame,
                                     variable=tk.DoubleVar(),
                                     orient=tk.HORIZONTAL,
                                     from_=0.001, to_=1.0, resolution=0.001,
                                     bg="#DDDDDD",
                                     activebackground="#FF0000",
                                     highlightcolor="#00FFFF",
                                     label="Learning Rate",
                                     command=lambda event: self.learning_rate_slider_callback())
        self.learning_rate_slider.set(self.learning_rate)
        self.learning_rate_slider.bind("<ButtonRelease-1>", lambda event: self.learning_rate_slider_callback())
        self.learning_rate_slider.grid(row=0, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        # set up the number of delayed elements slider
        self.delayed_elems_slider = tk.Scale(self.sliders_frame,
                                             variable=tk.IntVar(),
                                             orient=tk.HORIZONTAL,
                                             from_=0, to_=100, resolution=1,
                                             bg='#DDDDDD',
                                             activebackground='#FF0000',
                                             highlightcolor='#00FFFF',
                                             label='Delayed Elements',
                                             command=lambda event: self.delayed_elems_slider_callback())
        self.delayed_elems_slider.set(self.delayed_elems)
        self.delayed_elems_slider.bind('<ButtonRelease-1>', lambda event: self.delayed_elems_slider_callback())
        self.delayed_elems_slider.grid(row=1, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        # set up training sample size slider
        self.train_size_slider = tk.Scale(self.sliders_frame,
                                          variable=tk.IntVar(),
                                          orient=tk.HORIZONTAL,
                                          from_=0, to_=100, resolution=1.0,
                                          bg='#DDDDDD',
                                          activebackground='#FF0000',
                                          highlightcolor='#00FFFF',
                                          label='Training Sample Size',
                                          command=lambda event: self.train_size_slider_callback())
        self.train_size_slider.set(80)
        self.train_size_slider.bind('<ButtonRelease-1>', lambda event: self.train_size_slider_callback())
        self.train_size_slider.grid(row=2, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        # set up batch size slider
        self.batch_size_slider = tk.Scale(self.sliders_frame,
                                          variable=tk.IntVar(),
                                          orient=tk.HORIZONTAL,
                                          from_=1, to_=200, resolution=1,
                                          bg='#DDDDDD',
                                          activebackground='#FF0000',
                                          highlightcolor='#00FFFF',
                                          label='Batch Size',
                                          command=lambda event: self.batch_size_slider_callback())
        self.batch_size_slider.set(self.batch_size)
        self.batch_size_slider.bind('<ButtonRelease-1>', lambda event: self.batch_size_slider_callback())
        self.batch_size_slider.grid(row=3, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        # set up number of iterations slider
        self.num_iters_slider = tk.Scale(self.sliders_frame,
                                         variable=tk.IntVar(),
                                         orient=tk.HORIZONTAL,
                                         from_=1, to_=100, resolution=1,
                                         bg='#DDDDDD',
                                         activebackground='#FF0000',
                                         highlightcolor='#00FFFF',
                                         label='Number of Iterations',
                                         command=lambda event: self.num_iters_slider_callback())
        self.num_iters_slider.set(self.num_iters)
        self.num_iters_slider.bind('<ButtonRelease-1>', lambda event: self.num_iters_slider_callback())
        self.num_iters_slider.grid(row=4, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
        #########################################################################
        #  Set up the frame for button(s)
        #########################################################################

        self.buttons_frame = tk.Frame(self.master)
        self.buttons_frame.grid(row=0, column=1, sticky=tk.N + tk.E + tk.S + tk.W)
        self.buttons_frame.rowconfigure(0, weight=1)
        # self.buttons_frame.rowconfigure(1, weight=2)
        # self.buttons_frame.columnconfigure(0, weight=1, uniform='xx')
        self.reset_weights_btn = tk.Button(self.buttons_frame, text="Set Weights to Zero", command=self.reset_weights)
        self.reset_weights_btn.grid(row=0, column=1, sticky=tk.N + tk.E + tk.S + tk.W)
        self.train_button = tk.Button(self.buttons_frame, text="Adjust Weights (Train)", command=self.train_button_callback)
        self.train_button.grid(row=1, column=1, sticky=tk.N + tk.E + tk.S + tk.W)
        self.reset_weights()
        # self.display_activation_function()
        print("Window size:", self.master.winfo_width(), self.master.winfo_height())

    def display_activation_function(self):
        """
        Plots the decision boundary and correctly labels each side
        :return: None
        """
        """ Below code draws a figure onto a subplot
        """
        # PLOT MAE - PRICE
        self.mae_price_plt.cla()
        self.mae_price_plt.cla()
        self.mae_price_plt.plot(range(len(self.mae_price)), self.mae_price)
        self.mae_price_plt.set_ylim([self.ymin, self.ymax])
        plt.ylim(self.ymin, self.ymax)
        self.mae_price_plt.set_xlabel('Epochs')
        self.mae_price_plt.set_ylabel('Error')
        self.mae_price_plt.set_title("Maximum Absolute Error - Price")
        # TODO PLOT MAE - VOLUME
        self.mae_volume_plt.cla()
        self.mae_volume_plt.cla()
        self.mae_volume_plt.plot(range(len(self.mae_volume)), self.mae_volume)
        self.mae_volume_plt.set_ylim([self.ymin, self.ymax])
        plt.ylim(self.ymin, self.ymax)
        self.mae_volume_plt.set_xlabel('Epochs')
        self.mae_volume_plt.set_ylabel('Error')
        self.mae_volume_plt.set_title('Maximum Absolute Error - Volume')
        # TODO PLOT MSE - PRICE
        self.mse_price_plt.cla()
        self.mse_price_plt.cla()
        self.mse_price_plt.plot(range(len(self.mse_price)), self.mse_price)
        self.mse_price_plt.set_ylim([self.ymin, self.ymax])
        plt.ylim(self.ymin, self.ymax)
        self.mse_price_plt.set_xlabel('Epochs')
        self.mse_price_plt.set_ylabel('Error')
        self.mse_price_plt.set_title('Mean Squared Error - Price')
        # TODO PLOT MSE - VOLUME
        self.mse_volume_plt.cla()
        self.mse_volume_plt.cla()
        self.mse_volume_plt.plot(range(len(self.mse_volume)), self.mse_volume)
        self.mse_volume_plt.set_ylim([self.ymin, self.ymax])
        plt.ylim(self.ymin, self.ymax)
        self.mse_volume_plt.set_xlabel('Epochs')
        self.mse_volume_plt.set_ylabel('Error')
        self.mse_volume_plt.set_title('Mean Squared Error - Volume')
        self.canvas.draw()

    def train_button_callback(self):
        """
        Implements LMS algorithm for Widrow-Hoff Learning
        :return: None
        """
        num_batches = int(np.round(self.X_train.shape[0] / self.batch_size))
        print(self.batch_size)
        print(np.round(self.X_train.shape[0] / self.batch_size))
        print("self.X_train.shape[0]: ", self.X_train.shape[0])
        print("num_batches: ", num_batches)
        for iter in range(1, self.num_iters + 1):
            for batch in range(num_batches):
                x_train = self.X_train[batch:self.batch_size]
                price_train = self.price_train[batch:self.batch_size]
                volume_train = self.volume_train[batch:self.batch_size]
                for delay in range(self.delayed_elems):
                    x_train_delay = x_train[delay:self.delayed_elems]
                    price_delay = price_train[delay:self.delayed_elems]
                    volume_delay = volume_train[delay:self.delayed_elems]
                    self.price_ada.fit(x_train_delay, price_delay)
                    self.volume_ada.fit(x_train_delay, volume_delay)
                # for row, t_val in zip(x_train, price_train):
                #     activation = np.dot(self.weights[1:], row) + self.weights[0]
                #     update = t_val - activation
                #     self.weights[1:] += 2 * self.learning_rate * update * row
                #     self.weights[0] += 2 * self.learning_rate * update
                # self.price_ada.fit(x_train, price_train)
                # self.volume_ada.fit(x_train, volume_train)
                price_output = list()
                volume_output = list()
                for price in self.price_test:
                    price_output.append(self.price_ada.activation(price))
                for volume in self.volume_test:
                    volume_output.append(self.volume_ada.activation(volume))
                price_output = np.array(price_output)[:, 0]
                volume_output = np.array(volume_output)[:, 0]
                err_price = self.price_test - price_output
                err_volume = self.volume_test - volume_output
                self.mae_price.append(np.abs(err_price).max())
                self.mae_volume.append(np.abs(err_volume).max())
                self.mse_price.append(np.average(err_price ** 2))
                self.mse_volume.append(np.average(err_volume ** 2))
                self.display_activation_function()
            # print(np.dot(self.weights))
            # print(self.weights[1:].shape)
            # print(np.dot(self.weights[1:], self.price_test.reshape((self.price_test.shape, 1))) + self.weights[0])
            # for prices in self.price_test:
            #     # print(np.dot(self.weights[1:], prices) + self.weights[0])
            #     print(np.dot(self.weights[1:], prices).shape)
            # price_output = list()
            # volume_output = list()
            # for price in self.price_test:
            #     price_output.append(self.price_ada.activation(price))
            # for volume in self.volume_test:
            #     volume_output.append(self.volume_ada.activation(volume))
            # price_output = np.array(price_output)[:, 0]
            # volume_output = np.array(volume_output)[:, 0]
            # err_price = self.price_test - price_output
            # err_volume = self.volume_test - volume_output
            # self.mae_price.append(np.abs(err_price).max())
            # self.mae_volume.append(np.abs(err_volume).max())
            # self.mse_price.append(np.average(err_price ** 2))
            # self.mse_volume.append(np.average(err_volume ** 2))
            # self.display_activation_function()

    def reset_weights(self):
        """
        Resets weights and price dictionaries
        :return: None
        """
        self.price_ada.reset_weights(self.X_train)
        self.volume_ada.reset_weights(self.X_train)
        self.mae_price = []
        self.mae_volume = []
        self.mse_price = []
        self.mse_volume = []
        self.xmin = 1
        self.xmax = 10
        self.ymin = 0
        self.ymax = 2
        self.learning_rate = 0.1
        self.delayed_elems = 10
        self.train_size = 0.8
        self.batch_size = 100
        self.num_iters = 10
        # self.learning_rate_slider.set(self.learning_rate)
        # self.delayed_elems_slider.set(self.delayed_elems)
        # self.train_size_slider.set(self.train_size)
        # self.num_iters_slider.set(self.num_iters)
        # self.batch_size_slider.set(self.batch_size)
        self.mae_price_plt.cla()
        self.mae_price_plt.cla()
        self.mse_price_plt.cla()
        self.mse_price_plt.cla()
        self.mae_volume_plt.cla()
        self.mae_volume_plt.cla()
        self.mse_volume_plt.cla()
        self.mse_volume_plt.cla()

    def learning_rate_slider_callback(self):
        self.learning_rate = self.learning_rate_slider.get()
        self.price_ada.set_eta(self.learning_rate)
        self.volume_ada.set_eta(self.learning_rate)

    def delayed_elems_slider_callback(self):
        self.delayed_elems = self.delayed_elems_slider.get()
        self.weights = np.zeros(1 + self.X_train.shape[1])

    def train_size_slider_callback(self):
        self.train_size = self.train_size_slider.get() / 100.0
        self.X_train, self.X_test, self.price_train, self.price_test, self.volume_train, self.volume_test = \
            train_test_split(self.inp_vec, self.price_var, self.volume_var, train_size=self.train_size)
        print("self.price_train.shape: ", self.price_train.shape)

    def num_iters_slider_callback(self):
        self.num_iters = self.num_iters_slider.get()
        self.price_ada.set_iters(self.num_iters)
        self.volume_ada.set_iters(self.num_iters)

    def batch_size_slider_callback(self):
        self.batch_size = self.batch_size_slider.get()
