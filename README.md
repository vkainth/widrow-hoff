# Widrow-Hoff Learning
This repository is an implementation of Widrow-Hoff Learning as a submission towards the completion
CSE 5368 - Neural Networks. Widrow-Hoff Learning implements LMS learning algorithm to change the
weights and biases. The project is presented as a desktop GUI application.

## GUI Explanation
* Two buttons: one to reset weights and another to perform the learning
* A slider to change the number of delayed elements
* A slider to change the learning rate, alpha
* A slider to change the number of training samples to use
* A slider to change the batch size
* A slider to change the number of epochs to run the training for
* A plot area to display the error plots

## Data Explanation
The data is stock data provided by the intrustor. We had to guess the Volume of Stocks from their
Price and vice-versa.

## Error Plots
The mean-squared-error and the mean-absolute-error were to be plotted for both Price and Volume.

## Screenshots
![Empty GUI](01.png?raw=true "Empty GUI")
![After 10 epochs](02.png?raw=true "After 10 Epochs")