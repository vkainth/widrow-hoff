# Kainth, Vibhor
# 1001-420-430
# 2017-10-09
# Assignment_03_04

import numpy as np
import scipy.misc
import glob
import pandas as pd


def input_data(data_path):
    """
    Loads the provided file from the provided path
    :param data_path: path of directory of data file
    :return: numpy array of normalized values
    """
    data_file = pd.read_csv(data_path)
    data_file['Price'] = (data_file['Price'] / data_file['Price'].max()) - 0.5
    data_file['Volume'] = (data_file['Volume'] / data_file['Volume'].max()) - 0.5
    return data_file.values


if __name__ == '__main__':
    input_data("Data/stock_data.csv")
